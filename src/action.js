export const INCREMENT = "INCREMENT" 
export const DECREMENT = "DECREMENT "
export const PLUS_FIVE = "PLUS_FIVE"

export const actionCreator = (type, payload) => ({ type, payload })