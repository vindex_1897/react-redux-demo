import { createStore } from 'redux';
import appReducer from "./reducer"

//You can use redux dev tools to inspect app state

export const store = createStore(appReducer);