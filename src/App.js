import React from 'react';
// import logo from './logo.svg';
import { connect } from 'react-redux';
import './App.css';
import { actionCreator, INCREMENT, DECREMENT, PLUS_FIVE } from "./action"

const App = ({ count, plusFive, increment, decrement }) => {
  return (
    <div className="App">
      <header className="App-header">
      <div>
        <h2>Counter</h2>
        <div>
          <button onClick={decrement}>-</button>
          <span>{count}</span>
          <button onClick={increment}>+</button>
          <button onClick={plusFive}>+5</button>
        </div>
      </div>
      </header>
    </div>
  );
}

export default connect(props => ({...props}), dispatch => ({
  increment: () => dispatch(actionCreator(INCREMENT)),
  decrement: () => dispatch(actionCreator(DECREMENT)),
  plusFive: () => dispatch(actionCreator(PLUS_FIVE, 5))
}))(App)
