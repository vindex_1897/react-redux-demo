import { INCREMENT, DECREMENT, PLUS_FIVE } from "./action"

const initialState = {
    count: 0
};

//you can use combineReducer to combine reducers
const reducer = (state = initialState, action) => {
    switch(action.type){
        case INCREMENT: 
            return {
                count: state.count + 1
            };

        case DECREMENT: 
            return {
                count: state.count - 1
            }

        case PLUS_FIVE: 
            return {
                count: state.count + action.payload
            }

        default: return state;
    }
}

export default reducer